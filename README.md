# Helloworld JS app

A simple app to test docker, gitlab autodevop, cloudcode, ArgoCD to K8s

## Build Docker image
- `.gitlab-ci.yml`: will build the container, available at registry.gitlab.com/gregbkr/hello
- Test the image: `docker run -d --name hello -p 8080:8080 registry.gitlab.com/gregbkr/hello`, Browse http://localhost:8080

## Create the K8s manifest files
- In the folder `Kubernetes-manifest`, use Cloudcode to contruct these files
- Deploy with Cloudcode `Kubernetes - run app`

## CD with Argo
- [Install](https://argoproj.github.io/argo-cd/getting_started/) Argo in K8s and ArgoCli
- Create an app from the hello manifest: `argocd app create hello --repo https://gitlab.com/gregbkr/hello.git --path kubernetes-manifests --dest-namespace default --dest-server https://kubernetes.default.svc --directory-recurse`

- Sync app: `argocd app sync hello`
- Check app: `argocd app get hello`
- Connect to app: `kubectl port-forward svc/hello-service 3000:3000` and browse http://localhost:3000
- Delete app: `argocd app delete hello`

